setTimeout(() => {
    console.log('%c⚠~w(>///<)w ~', 'color: #F08080; font-size: 24px; text-shadow: 1px 1px black;');
    console.log('%c使用这里的东东可能会被坏坏偷走信息哦(＾○＾)请不要输入或者复制不懂的代码吖~', 'color: #bf9c9c; font-size: 14px; text-shadow: 1px 1px black;');
    console.log('%c如果你不确定自己在干什么，就关掉这个页面吧（￣︶￣）↗', 'color: #F08080; font-size: 14px; text-shadow: 1px 1px black;');
},3000);

let apiUrl = 'https://p0.kamiya.dev', isDev = false;

if(!window.location.href.match('kamiya.dev') && !window.location.href.match('closeai.us')) {
    apiUrl = 'http://127.0.0.1:11681';
    isDev = true;
}

fetch(apiUrl + '/api/checkNetwork').then((res) => {}).catch((err) => {
    console.log(err);
    apiUrl = 'https://p0.kamiya.dev';
    fetch(apiUrl + '/api/checkNetwork').then((res) => {}).catch((err) => {
        console.log(err);
        apiUrl = 'https://b1.kmya.pro';
    });
});

const i18ncache = {};

function geti18nJSON(url, callback) {
    if(i18ncache[url]) return callback(i18ncache[url]);
    $.getJSON(url, function(data) {
        i18ncache[url] = data;
        callback(data);
    });
}

function applyi18n(lang, index) {
    geti18nJSON(`./i18n/${lang}/${index}.json`, function(data) {
        if(data.title) document.title = data.title;
        $("[data-i18n]").each(function() {
            const key = $(this).data("i18n");
            if(!data[key]) return;
            $(this).text(data[key]);
        });
    });
}

const rawmutation = mdui.mutation;

function usei18n(index) {
    let language = localStorage.getItem('language') || (navigator.language || navigator.userLanguage).toLowerCase();
    if(language === 'zh-cn') return;
    $.getJSON('./i18n/config.json', function(data) {
       if(data.lang.indexOf(language) < 0) language = "en";
       applyi18n(language, 'sidebar');
       applyi18n(language, 'footer');
       applyi18n(language, index);
        mdui.mutation = function() {
            rawmutation();
            usei18n(index);
        }
    });
}

function getSessionToken () {
    return localStorage.getItem('sessionToken');
}

function removeSessionToken () {
    localStorage.removeItem('sessionToken');
}

function setSessionToken (token) {
    localStorage.setItem('sessionToken', token);
}

function getCookie(cname) {
    return localStorage.getItem(cname);
}

function setCookie(cname,cvalue,exdays) {
    localStorage.setItem(cname, cvalue);
    if(exdays <= 0) localStorage.removeItem(cname);
}

const passwordCheck = (password) => {
    if(password.length < 8) return false;
    if(!password.match(/[a-z]/)) return false;
    if(!password.match(/[A-Z]/)) return false;
    if(!password.match(/[0-9]/)) return false;
    return true;
}

function newCaptchaPromise() {
    return new Promise((resolve, reject) => {
        var dialog = mdui.dialog({
            title: '人机验证（仅需完成其中之一）',
            content: '<div id="challenge"></div><br><div id="challenge1"></div><br><div id="hcaptcha"></div><br><br><br><br><br><br><br><br>'
        });

        hcaptcha.render('hcaptcha', {
            sitekey: '8798104b-4b96-435f-a2f0-250709ba325d',
            callback: function (token){
                dialog.close();
                resolve({
                    token: token,
                    type: 'hcaptcha'
                });
            }
        });

        turnstile.render('#challenge', {
            sitekey: '0x4AAAAAAABiilVDvePhRHqz',
            callback: function(token){
                dialog.close();
                resolve({
                    token: token,
                    type: 'cloudflare'
                });
            }
        });

        grecaptcha.render('challenge1', {
            sitekey: '6LcufGwjAAAAADFyL2mjMRKR3I09BF5TvmB5PVZv',
            callback: function(token) {
                dialog.close();
                resolve({
                    token: token,
                    type: 'google'
                });
            },
        });
    });
}

function newCaptchaCallback(callback) {
    var dialog = mdui.dialog({
        title: '人机验证（仅需完成其中之一）',
        content: '<div id="challenge"></div><br><div id="challenge1"></div><br><div id="hcaptcha"></div><br><br><br><br><br><br><br><br>'
    });

    hcaptcha.render('hcaptcha', {
        sitekey: isDev ? '10000000-ffff-ffff-ffff-000000000001' : '8798104b-4b96-435f-a2f0-250709ba325d',
        callback: function (token){
            dialog.close();
            callback({
                token: token,
                type: 'hcaptcha'
            }, dialog);
        }
    });

    turnstile.render('#challenge', {
        sitekey: '0x4AAAAAAABiilVDvePhRHqz',
        callback: function(token){
            dialog.close();
            callback({
                token: token,
                type: 'cloudflare'
            }, dialog);
        }
    });

    grecaptcha.render('challenge1', {
        sitekey: '6LcufGwjAAAAADFyL2mjMRKR3I09BF5TvmB5PVZv',
        callback: function(token) {
            dialog.close();
            callback({
                token: token,
                type: 'google'
            }, dialog);
        },
    });
}
